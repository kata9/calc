package ru.abramov.calc;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);

        String a = scanner.nextLine().replaceAll(" ", "");
        String[] arrayStrings = a.split("\\W");
        String stringForChar = a.replaceAll("\\w", "");

        if (stringForChar.toCharArray().length > 1) {
            throw new Exception("Формат математической операции не удовлетворяет требованиям.");
        } else if (stringForChar.toCharArray().length == 0) {
            throw new Exception("Строка не является математической операцией.");
        }

        convertToInt(arrayStrings, stringForChar);
    }

    private static void convertToInt(String[] arrayStrings, String symbol) throws Exception {
        Integer[] out = new Integer[2];
        int cnt = 0;
        boolean isRoman = false;

        for (String str : arrayStrings) {
            if (str.matches("\\d+")) {
                cnt++;
            }
        }

        if (cnt % 2 != 0) {
            throw new Exception("Используются разные системы счисления одновременно.");
        }

        cnt = 0;
        for (String str : arrayStrings) {
            if (str.matches("(10|[0-9])")) {
                out[cnt] = Integer.parseInt(str);
                cnt++;
            } else if (str.matches("\\d+")) {
                throw new Exception("Число " + str + " за пределами условий");
            } else {
                out[cnt] = romanToArab(str);
                cnt++;
                isRoman = true;
            }
        }

        int result;
        result = calculate(out[0], out[1], symbol);

        if (isRoman) {
            System.out.println(arabToRoman(result));
        } else {
            System.out.println(result);
        }

    }

    private static int calculate(Integer firstInt, Integer secondInt, String simbol) throws Exception {
        if (simbol.equals("+")) {
            return firstInt + secondInt;
        } else if (simbol.equals("-")) {
            return firstInt - secondInt;
        } else if (simbol.equals("*")) {
            return firstInt * secondInt;
        } else if (simbol.equals("/")) {
            return firstInt / secondInt;
        } else {
            throw new Exception("Мы не обрабатываем вычисления: " + simbol);
        }
    }

    private static int romanToArab(String in) throws Exception {
        switch (in) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
            case "IX":
                return 9;
            case "X":
                return 10;
            default:
                throw new Exception("Число " + in + " за пределами условий");
        }
    }

    private static String arabToRoman(int in) throws Exception {
        if (in < 0) {
            throw new Exception("В римской системе счисления нет отрицательных чисел");
        } else if (in > 100) {
            throw new Exception("Значение за пределами условий задачи");
        }
        return getNum(in);
    }

    private static String getNum(Integer n) {
        switch (n) {
            case 1:
                return "I";
            case 4:
                return "IV";
            case 40:
                return "XL";
            case 50:
                return "L";
            case 90:
                return "XC";
            case 100:
                return "C";
            case 5:
                return "V";
            case 9:
                return "IX";
            case 10:
                return "X";
            default:
                return calculateRoman(n);
        }
    }

    private static String calculateRoman(Integer n) {
        String out = "";
        if (n < 4) {
            for (int i = 0; i < n; i++) {
                out = out + "I";
            }
            return out;
        } else if (n > 5 && n < 9) {
            out = "V";
            n = n - 5;
            for (int i = 0; i < n; i++) {
                out = out + "I";
            }
            return out;
        } else if (n > 10) {
            int dozens = n / 10;
            String units = getNum(n % 10);

            if (dozens < 5) {
                for (int i = 0; i < dozens; i++) {
                    out = out + "X";
                }
            } else {
                dozens = dozens - 5;
                out = "L";
                for (int i = 0; i < dozens; i++) {
                    out = out + "X";
                }
            }


            return out + units;
        }
        return null;
    }

}
